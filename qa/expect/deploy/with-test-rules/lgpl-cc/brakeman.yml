# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# rule-set version: 1.0.0
# yamllint enable
---
rules:
- id: brakeman.ruby_cookie_rule-CheckCookieStoreSessionSecurityAttributes
  patterns:
  - pattern-either:
    - patterns:
      - pattern: ":$KEY => false\n"
      - pattern-inside: 'ActionController::Base.session = {...}

          '
    - pattern: "$MODULE::Application.config.session_store :cookie_store, ..., :$KEY
        => false, ...\n"
    - pattern: "$CLASS.application.config.session_store :cookie_store, ..., $KEY:
        false, ...\n"
  - metavariable-regex:
      metavariable: "$KEY"
      regex: "^(session_)?(http_?only|secure)$"
  message: "The detected issue pertains to a Rails application where the session \nconfiguration,
    specifically using cookie_store, has been identified \nwith the $KEY attribute
    set to false. This setting is potentially \ninsecure because it may relate to
    crucial security attributes such \nas HttpOnly or Secure flags not being enforced.
    In the context of \nweb applications, these flags play a vital role in enhancing
    session \nsecurity:\n\n- HttpOnly Flag: When enabled, this flag prevents client-side
    scripts\nfrom accessing the cookie. This mitigation is crucial for reducing \nthe
    risk of cross-site scripting (XSS) attacks, where an attacker \nmight attempt
    to steal session cookies.\n- Secure Flag: This flag ensures that cookies are sent
    over secure, \nencrypted connections only (HTTPS). It's a critical security measure
    \nthat helps prevent cookies from being intercepted by attackers when \ntransmitted
    over an unencrypted connection.\n\nTo mitigate the identified security risk and
    ensure that your Rails \napplication's session cookies are securely configured.
    \n\nSecure Code Example:\n```\n# config/initializers/session_store.rb\nRails.application.config.session_store
    :cookie_store, key: '_your_app_session',\n                                                  httponly:
    true,\n                                                  secure: Rails.env.production?\n```\n"
  languages:
  - ruby
  severity: INFO
  metadata:
    category: security
    shortDescription: Sensitive cookie without 'HttpOnly' and 'Secure' flags
    owasp:
    - A6:2017-Security Misconfiguration
    - A05:2021-Security Misconfiguration
    cwe: CWE-1004
    technology:
    - ruby
    - rails
    security-severity: LOW
    references:
    - https://owasp.org/www-project-web-security-testing-guide/latest/4-Web_Application_Security_Testing/06-Session_Management_Testing/02-Testing_for_Cookies_Attributes
    - https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/brakeman/check-cookie-store-session-security-attributes.yaml
    primary_identifier: brakeman.ruby_cookie_rule-CheckCookieStoreSessionSecurityAttributes
    secondary_identifiers:
    - name: Brakeman Warning Code 27
      type: brakeman_warning_code
      value: '27'
- id: brakeman.ruby_crypto_rule-InsufficientRSAKeySize
  message: "The RSA key size $SIZE is insufficent by NIST standards. It is \nrecommended
    to use a key length of 2048 or higher. The RSA algorithm, \nbeing widely used
    for secure data transmission, relies on key size \nfor its security. Smaller key
    sizes (e.g., 1024 bits and below) are \nmore susceptible to brute-force attacks,
    where an attacker uses \ncomputational power to decrypt data or forge signatures.
    \n\nTo address this security concern, ensure that all RSA keys are \ngenerated
    with a size of 2048 bits or more.\n\nFolowing is a secure code example:\n```\nrequire
    'openssl'\n\n# Generate a new RSA key of 2048 bits\nrsa_key = OpenSSL::PKey::RSA.new(2048)\n\n#
    To export the RSA key\nprivate_key = rsa_key.to_pem\npublic_key = rsa_key.public_key.to_pem\n```\n"
  languages:
  - ruby
  severity: WARNING
  metadata:
    category: security
    shortDescription: Inadequate encryption strength
    owasp:
    - A3:2017-Sensitive Data Exposure
    - A02:2021-Cryptographic Failures
    cwe: CWE-326
    technology:
    - ruby
    security-severity: MEDIUM
    references:
    - https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/insufficient-rsa-key-size.yaml
    - https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-57Pt3r1.pdf
    primary_identifier: brakeman.ruby_crypto_rule-InsufficientRSAKeySize
    secondary_identifiers:
    - name: Brakeman Warning Code 128
      type: brakeman_warning_code
      value: '128'
  patterns:
  - pattern-either:
    - pattern: OpenSSL::PKey::RSA.generate($SIZE,...)
    - pattern: OpenSSL::PKey::RSA.new($SIZE, ...)
    - patterns:
      - pattern-either:
        - patterns:
          - pattern-inside: |
              $ASSIGN = $SIZE
              ...
          - pattern-either:
            - pattern: OpenSSL::PKey::RSA.new($ASSIGN, ...)
            - pattern: OpenSSL::PKey::RSA.generate($ASSIGN, ...)
        - patterns:
          - pattern-inside: |
              def $METHOD1(...)
              ...
              $ASSIGN = $SIZE
              ...
              end
              ...
          - pattern-either:
            - pattern: OpenSSL::PKey::RSA.new($ASSIGN, ...)
            - pattern: OpenSSL::PKey::RSA.generate($ASSIGN, ...)
  - metavariable-comparison:
      metavariable: "$SIZE"
      comparison: "$SIZE < 2048"
