# yamllint disable
# License: GNU Lesser General Public License v3.0
# rule-set version: 1.0.0
# yamllint enable
---
rules:
- id: mobsf.swift-other-rule-ios_biometric_acl
  patterns:
  - pattern-either:
    - pattern: ".biometryAny"
    - pattern: ".userPresence"
    - pattern: ".touchIDAny"
    - pattern: SecAccessControlCreateWithFlags(...)
  message: "Weak biometric ACL flag is associated with a key stored in Keychain. \nWith
    '.biometryAny/.userPresence/.touchIDAny' flag, an attacker with \nthe ability
    to add a biometry to the device can authenticate as the \nuser. It is recommended
    to use more specific and secure authentication \nmechanisms like '.biometryCurrentSet'
    and '.touchIDCurrentSet'.\n\nHere's an example of how to fix the problem by using
    .biometryCurrentSet \nfor biometric authentication in Swift:\n```\nimport LocalAuthentication\n\n//
    Create an instance of LAContext for biometric authentication\nlet context = LAContext()\n\n//
    Check if biometric authentication is available\nif context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
    error: nil) {\n  // Use biometryCurrentSet for biometric authentication\n  context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
    \n        localizedReason: \"Authenticate with biometrics\", \n        reply:
    { success, error in\n            if success {\n              print(\"Biometric
    authentication successful.\")\n              // Proceed with authenticated actions\n
    \           } else {\n              print(\"Biometric authentication failed: \n
    \                   \\(error?.localizedDescription ?? \"Unknown error\")\")\n
    \             // Handle authentication failure\n            }\n  })\n} else {\n
    \ print(\"Biometric authentication not available.\")\n  // Fallback to alternative
    authentication method\n}\n\n```\n"
  languages:
  - swift
  severity: ERROR
  metadata:
    cwe: CWE-305
    shortDescription: Authentication bypass by primary weakness
    owasp:
    - A2:2017-Broken Authentication
    - A7:2021-Identification and Authentication Failures
    security-severity: CRITICAL
    primary_identifier: mobsf.swift-other-rule-ios_biometric_acl
    secondary_identifiers:
    - name: mobsf ID swift-other-rule-ios_biometric_acl
      type: mobsf_rule_type
      value: swift-other-rule-ios_biometric_acl
- id: mobsf.swift-other-rule-ios_dtls1_used
  patterns:
  - pattern: "$Y.TLSMinimumSupportedProtocolVersion"
  - pattern-inside: |
      ...
      $X = "tls_protocol_version_t.DTLSv10"
      ...
  message: "DTLS 1.2 should be used. Detected old version - DTLS 1.0.\nDTLS (Datagram
    Transport Layer Security) 1.0 suffers from \nvarious security vulnerabilities
    and weaknesses, as it is \nan outdated and less secure protocol compared to newer
    \nversions such as DTLS 1.2 or 1.3.\n\nHere's an example of how to use DTLS 1.2:\n```\nimport
    Network\n\n// Create a NWConnection instance with DTLS 1.2\nlet connection = NWConnection(host:
    NWEndpoint.Host(\"example.com\"), port: NWEndpoint.Port(\"443\"), using: .dtls)\n\n//
    Start the connection\nconnection.start(queue: .main)\n\n// Handle connection state
    changes\nconnection.stateUpdateHandler = { newState in\n  switch newState {\n
    \ case .ready:\n    print(\"Connection ready.\")\n  // Perform data transfer or
    other operations\n  case .failed(let error):\n    print(\"Connection failed with
    error: \\(error)\")\n  default:\n    break\n  }\n}\n```\n"
  languages:
  - swift
  severity: WARNING
  metadata:
    cwe: CWE-757
    shortDescription: Selection of less-secure algorithm during negotiation ('algorithm
      downgrade')
    owasp:
    - A6:2017-Security Misconfiguration
    - A5:2021-Security Misconfiguration
    security-severity: MEDIUM
    primary_identifier: mobsf.swift-other-rule-ios_dtls1_used
    secondary_identifiers:
    - name: mobsf ID swift-other-rule-ios_dtls1_used
      type: mobsf_rule_type
      value: swift-other-rule-ios_dtls1_used
