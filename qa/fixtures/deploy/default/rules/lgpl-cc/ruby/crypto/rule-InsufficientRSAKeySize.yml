# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_crypto_rule-InsufficientRSAKeySize"
  message: |
    The RSA key size $SIZE is insufficent by NIST standards. It is 
    recommended to use a key length of 2048 or higher. The RSA algorithm, 
    being widely used for secure data transmission, relies on key size 
    for its security. Smaller key sizes (e.g., 1024 bits and below) are 
    more susceptible to brute-force attacks, where an attacker uses 
    computational power to decrypt data or forge signatures. 

    To address this security concern, ensure that all RSA keys are 
    generated with a size of 2048 bits or more.

    Folowing is a secure code example:
    ```
    require 'openssl'

    # Generate a new RSA key of 2048 bits
    rsa_key = OpenSSL::PKey::RSA.new(2048)

    # To export the RSA key
    private_key = rsa_key.to_pem
    public_key = rsa_key.public_key.to_pem
    ```
  languages: 
  - "ruby"
  severity: "WARNING"
  metadata:
    category: "security"
    shortDescription: "Inadequate encryption strength"
    owasp:
    - "A3:2017-Sensitive Data Exposure"
    - "A02:2021-Cryptographic Failures"
    cwe: "CWE-326"
    technology:
    - "ruby"
    security-severity: "MEDIUM"
    references:
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/insufficient-rsa-key-size.yaml"
    - "https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-57Pt3r1.pdf"
  patterns:
  - pattern-either:
    - pattern: OpenSSL::PKey::RSA.generate($SIZE,...)
    - pattern: OpenSSL::PKey::RSA.new($SIZE, ...)
    - patterns:
      - pattern-either:
        - patterns:
          - pattern-inside: |
              $ASSIGN = $SIZE
              ...
          - pattern-either:
            - pattern: OpenSSL::PKey::RSA.new($ASSIGN, ...)
            - pattern: OpenSSL::PKey::RSA.generate($ASSIGN, ...)
        - patterns:
          - pattern-inside: |
              def $METHOD1(...)
              ...
              $ASSIGN = $SIZE
              ...
              end
              ...
          - pattern-either:
            - pattern: OpenSSL::PKey::RSA.new($ASSIGN, ...)
            - pattern: OpenSSL::PKey::RSA.generate($ASSIGN, ...)
  - metavariable-comparison:
      metavariable: $SIZE
      comparison: $SIZE < 2048