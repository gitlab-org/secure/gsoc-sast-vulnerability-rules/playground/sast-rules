# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/mobsf/mobsfscan/blob/main/mobsfscan/rules/patterns/ios/swift/swift_rules.yaml
# hash: e29e85c3
# yamllint enable
---
rules:
  - id: rules_lgpl_swift_other_rule-ios-biometric-acl
    patterns:
      - pattern-either:
          - pattern: .biometryAny
          - pattern: .userPresence
          - pattern: .touchIDAny
          - pattern: SecAccessControlCreateWithFlags(...)
    message: |
      Weak biometric ACL flag is associated with a key stored in Keychain. 
      With '.biometryAny/.userPresence/.touchIDAny' flag, an attacker with 
      the ability to add a biometry to the device can authenticate as the 
      user. It is recommended to use more specific and secure authentication 
      mechanisms like '.biometryCurrentSet' and '.touchIDCurrentSet'.
      
      Here's an example of how to fix the problem by using .biometryCurrentSet 
      for biometric authentication in Swift:
      ```
      import LocalAuthentication
  
      // Create an instance of LAContext for biometric authentication
      let context = LAContext()
      
      // Check if biometric authentication is available
      if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
        // Use biometryCurrentSet for biometric authentication
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, 
              localizedReason: "Authenticate with biometrics", 
              reply: { success, error in
                  if success {
                    print("Biometric authentication successful.")
                    // Proceed with authenticated actions
                  } else {
                    print("Biometric authentication failed: 
                          \(error?.localizedDescription ?? "Unknown error")")
                    // Handle authentication failure
                  }
        })
      } else {
        print("Biometric authentication not available.")
        // Fallback to alternative authentication method
      }

      ```
    languages:
      - swift
    severity: ERROR
    metadata:
      category: security
      cwe: "CWE-305"
      shortDescription: "Authentication bypass by primary weakness"
      owasp:
        - "A2:2017-Broken Authentication"
        - "A07:2021-Identification and Authentication Failures"
      security-severity: CRITICAL