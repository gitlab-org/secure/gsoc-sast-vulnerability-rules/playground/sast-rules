# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# ruleid: ruby_routes_rule-AvoidDefaultRoutes
map.connect ":controller/:action/:id"