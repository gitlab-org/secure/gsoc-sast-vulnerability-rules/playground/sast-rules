# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_eval_rule-NoEval"
  message: |
    The `eval` method in Ruby executes a string argument as Ruby code. When 
    `eval` is used with input that can be controlled or manipulated by an 
    external user, it can allow arbitrary code execution. This 
    means an attacker could potentially execute malicious code on the server, 
    leading to unauthorized access, data leakage, or server compromise.

    Remediation Steps:
    - Validate and sanitize input: If there's an absolute necessity to use 
    `eval`, ensure that any user input is rigorously validated and sanitized 
    to remove potentially harmful code. However, this is generally not 
    recommended due to the difficulty of securely sanitizing code.
    - Use safer alternatives: Depending on the requirement, consider using 
    safer alternatives to `eval`, such as `send` for calling methods dynamically 
    or employing DSLs (Domain-Specific Languages) and safe parsing libraries 
    designed for specific tasks.
    
    Secure Code Example:
    ```
    class Calculator
      def add(a, b)
        a + b
      end
    end

    # Safer alternative using send
    calculator = Calculator.new
    method = params[:operation] # Example: 'add' or 'subtract'
    a = params[:a].to_i
    b = params[:b].to_i

    if calculator.respond_to?(method)
      result = calculator.send(method, a, b)
    else
      puts "Invalid operation"
    end
    ```

    In this example, `send` is used to dynamically call a method on the 
    Calculator object based on user input. This approach is safer than 
    eval because it strictly limits the operations to those defined in 
    the class, preventing arbitrary code execution.
  severity: "ERROR"
  metadata:
    cwe: "CWE-95"
    shortDescription: "Improper neutralization of directives in dynamically evaluated
      code ('Eval Injection')"
    category: "security"
    owasp:
    - "A1:2017-Injection"
    - "A03:2021-Injection"
    security-severity: "HIGH"
    references:
    - "https://owasp.org/Top10/A03_2021-Injection"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/no-eval.yaml"
    technology:
    - "rails"
  languages: 
  - "ruby"
  mode: taint
  pattern-sources:
  - pattern-either:
    - pattern: params
    - pattern: cookies
    - patterns:
      - pattern: |
          RubyVM::InstructionSequence.compile(...)
      - pattern-not: |
          RubyVM::InstructionSequence.compile("...")
  pattern-sinks:
  - patterns:
    - pattern-either:
      - pattern: $X.eval
      - pattern: $X.class_eval
      - pattern: $X.instance_eval
      - pattern: $X.module_eval
      - pattern: $X.eval(...)
      - pattern: $X.class_eval(...)
      - pattern: $X.instance_eval(...)
      - pattern: $X.module_eval(...)
      - pattern: eval(...)
      - pattern: class_eval(...)
      - pattern: module_eval(...)
      - pattern: instance_eval(...)
    - pattern-not: $M("...",...)