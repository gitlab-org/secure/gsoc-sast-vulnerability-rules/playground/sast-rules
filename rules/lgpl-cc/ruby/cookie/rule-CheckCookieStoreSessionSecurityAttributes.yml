# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_cookie_rule-CheckCookieStoreSessionSecurityAttributes"
  patterns:
  - pattern-either:
    - patterns:
      - pattern: |
          :$KEY => false
      - pattern-inside: |
          ActionController::Base.session = {...}
    - pattern: |
        $MODULE::Application.config.session_store :cookie_store, ..., :$KEY => false, ...
    - pattern: |
        $CLASS.application.config.session_store :cookie_store, ..., $KEY: false, ...
  - metavariable-regex:
      metavariable: $KEY
      regex: ^(session_)?(http_?only|secure)$
  message: |
    The detected issue pertains to a Rails application where the session 
    configuration, specifically using cookie_store, has been identified 
    with the $KEY attribute set to false. This setting is potentially 
    insecure because it may relate to crucial security attributes such 
    as HttpOnly or Secure flags not being enforced. In the context of 
    web applications, these flags play a vital role in enhancing session 
    security:

    - HttpOnly Flag: When enabled, this flag prevents client-side scripts
    from accessing the cookie. This mitigation is crucial for reducing 
    the risk of cross-site scripting (XSS) attacks, where an attacker 
    might attempt to steal session cookies.
    - Secure Flag: This flag ensures that cookies are sent over secure, 
    encrypted connections only (HTTPS). It's a critical security measure 
    that helps prevent cookies from being intercepted by attackers when 
    transmitted over an unencrypted connection.

    To mitigate the identified security risk and ensure that your Rails 
    application's session cookies are securely configured. 
    
    Secure Code Example:
    ```
    # config/initializers/session_store.rb
    Rails.application.config.session_store :cookie_store, key: '_your_app_session',
                                                      httponly: true,
                                                      secure: Rails.env.production?
    ```
  languages:
  - "ruby"
  severity: "INFO"
  metadata:
    category: "security"
    shortDescription: "Sensitive cookie without 'HttpOnly' and 'Secure' flags"
    owasp:
    - "A6:2017-Security Misconfiguration"
    - "A05:2021-Security Misconfiguration"
    cwe: "CWE-1004"
    technology:
    - "ruby"
    - "rails"
    security-severity: "LOW"
    references:
    - "https://owasp.org/www-project-web-security-testing-guide/latest/4-Web_Application_Security_Testing/06-Session_Management_Testing/02-Testing_for_Cookies_Attributes"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/brakeman/check-cookie-store-session-security-attributes.yaml"

