# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable

rules:
  - id: "java_endpoint_rule-ManuallyConstructedURLs"
    languages:
      - java
    severity: ERROR
    message: |
      User data flows into the host portion of this manually-constructed URL.
      This could allow an attacker to send data to their own server, potentially
      exposing sensitive data such as cookies or authorization information sent
      with this request. They could also probe internal servers or other
      resources that the server running this code can access. (This is called
      server-side request forgery, or SSRF.) Do not allow arbitrary hosts.
      Instead, create an allowlist for approved hosts hardcode the correct host,
      or ensure that the user data can only affect the path or parameters.

      Example of using allowlist:
      ```
      ArrayList<String> allowlist = (ArrayList<String>)
          Arrays.asList(new String[] { "https://example.com/api/1", "https://example.com/api/2", "https://example.com/api/3"});

      if(allowlist.contains(url)){
        ...
      }
      ```
    options:
      interfile: true
    metadata:
      security-severity: CRITICAL
      cwe: "CWE-918"
      owasp:
        - "A1:2017-Injection"
        - "A10:2021-Server-Side Request Forgery"
      shortDescription: "Detect manually constructed URLs"
      references:
        - https://cheatsheetseries.owasp.org/cheatsheets/Server_Side_Request_Forgery_Prevention_Cheat_Sheet.html
      category: security
      technology:
        - java
        - spring
      cwe2022-top25: true
      cwe2021-top25: true
      subcategory:
        - vuln
      impact: MEDIUM
      likelihood: MEDIUM
      confidence: MEDIUM
      interfile: true
      license: Commons Clause License Condition v1.0[LGPL-2.1-only]
      vulnerability_class:
        - Server-Side Request Forgery (SSRF)
    mode: taint
    pattern-sources:
      - patterns:
          - pattern-either:
              - pattern-inside: |
                  $METHODNAME(..., @$REQ(...) $TYPE $SOURCE,...) {
                    ...
                  }
              - pattern-inside: |
                  $METHODNAME(..., @$REQ $TYPE $SOURCE,...) {
                    ...
                  }
          - metavariable-regex:
              metavariable: $TYPE
              regex: ^(?!(Integer|Long|Float|Double|Char|Boolean|int|long|float|double|char|boolean))
          - metavariable-regex:
              metavariable: $REQ
              regex: (RequestBody|PathVariable|RequestParam|RequestHeader|CookieValue|ModelAttribute)
          - focus-metavariable: $SOURCE
    pattern-sinks:
      - pattern-either:
          - pattern: new URL($ONEARG)
          - patterns:
              - pattern-either:
                  - pattern: |
                      "$URLSTR" + ...
                  - pattern: |
                      "$URLSTR".concat(...)
                  - patterns:
                      - pattern-inside: |
                          StringBuilder $SB = new StringBuilder("$URLSTR");
                          ...
                      - pattern: $SB.append(...)
                  - patterns:
                      - pattern-inside: |
                          $VAR = "$URLSTR";
                          ...
                      - pattern: $VAR += ...
                  - patterns:
                      - pattern: String.format("$URLSTR", ...)
                      - pattern-not: String.format("$URLSTR", "...", ...)
                  - patterns:
                      - pattern-inside: |
                          String $VAR = "$URLSTR";
                          ...
                      - pattern: String.format($VAR, ...)
              - metavariable-regex:
                  metavariable: $URLSTR
                  regex: http(s?)://%(v|s|q).*
    pattern-sanitizers:
      - patterns:
          - pattern-either:
              - pattern: |
                  if($VALIDATION){
                    ...
                    new URL($ONEARG);
                    ...
                  } 
              - pattern: |
                  $A = $VALIDATION;
                  ...
                  if($A){
                    ...
                    new URL($ONEARG);
                    ...
                  }
          - metavariable-pattern:
              metavariable: $VALIDATION
              pattern-either:
                - pattern: |
                    $AL.contains(...)  
                - pattern: |
                    $AL.indexOf(...) != -1
