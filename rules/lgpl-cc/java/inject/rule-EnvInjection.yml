# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
rules:
  - id: "java_inject_rule-EnvInjection"
    message: |
      Detected input from a HTTPServletRequest going into the environment
      variables of an 'exec' command. The user input is passed directly to
      the Runtime.exec() function to set an environment variable. This allows 
      malicious input from the user to modify the command that will be executed.
      To remediate this, do not pass user input directly to Runtime.exec().
      Validate any user input before using it to set environment variables 
      or command arguments. Consider using an allow list of allowed values
      rather than a deny list. If dynamic commands must be constructed, use
      a map to look up valid values based on user input instead of using 
      the input directly.
      Example of safely executing an OS command:
      ```
      public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String param = "";
        if (request.getHeader("UserDefined") != null) {
            param = request.getHeader("UserDefined");
        }

        param = java.net.URLDecoder.decode(param, "UTF-8");
        String cmd = "/bin/cmd";

        String[] allowList = {"FOO=true","FOO=false","BAR=true", "BAR=false"}
        if(Arrays.asList(allowList).contains(param)){
            String[] argsEnv = {param};
        }
        
        Runtime r = Runtime.getRuntime();

        try {
            Process p = r.exec(cmd, argsEnv);
            printOSCommandResults(p, response);        
        } catch (IOException e) {
            System.out.println("Problem executing command");
            response.getWriter()
                    .println(org.owasp.esapi.ESAPI.encoder().encodeForHTML(e.getMessage()));
            return;
        }
      ```
    languages:
      - "java"
    severity: "ERROR"
    mode: "taint"
    pattern-sources:
      - patterns:
          - pattern-either:
              - pattern: |
                  (HttpServletRequest $REQ)
      - patterns:
          - pattern-inside: |
              $FUNC(..., $VAR, ...) {
              ...
              }
          - pattern: $VAR
    pattern-sinks:
      - pattern-either:
          - patterns:
              - pattern: (java.lang.Runtime $R).exec($CMD, $ENV_ARGS, ...);
              - focus-metavariable: $ENV_ARGS
          - patterns:
              - pattern: (ProcessBuilder $PB).environment().put($...ARGS);
              - focus-metavariable: $...ARGS
          - patterns:
              - pattern: |
                  $ENV = (ProcessBuilder $PB).environment();
                  ...
                  $ENV.put($...ARGS);
              - focus-metavariable: $...ARGS
    pattern-sanitizers:
      - patterns:
          - pattern-either:
              - pattern: |
                  if($VALIDATION){
                  ...
                  }
              - patterns:
                  - pattern-inside: |
                      $A = $VALIDATION;
                      ...
                  - pattern: |
                      if($A){
                      ...
                      }
          - metavariable-pattern:
              metavariable: $VALIDATION
              pattern-either:
                - pattern: |
                    $AL.contains(...)
    metadata:
      shortDescription:
        "Improper neutralization of special elements used in an OS
        command ('OS Command Injection')"
      category: "security"
      technology:
        - "java"
      cwe: CWE-78
      owasp:
        - "A1:2017-Injection"
        - "A03:2021-Injection"
      references:
        - "https://owasp.org/Top10/A03_2021-Injection"
      subcategory:
        - "vuln"
      likelihood: "MEDIUM"
      impact: "MEDIUM"
      confidence: "MEDIUM"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "Other"
      security-severity: "HIGH"
