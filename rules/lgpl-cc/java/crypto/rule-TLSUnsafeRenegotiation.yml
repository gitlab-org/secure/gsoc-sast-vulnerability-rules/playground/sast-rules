# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-stdlib/tls-renegotiation.yaml
# yamllint enable
---
rules:
  - id: java_crypto_rule-TLSUnsafeRenegotiation
    languages:
      - "java"
    patterns:
      - pattern: |
          java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", $TRUE);
      - metavariable-pattern:
          metavariable: $TRUE
          pattern-either: 
            - pattern: |
                true
            - pattern: |
                "true"
            - pattern: |
                Boolean.TRUE
    message: |
      This code enables unsafe renegotiation in SSL/TLS connections, which is
      vulnerable to man-in-the-middle attacks. In such attacks, an attacker
      could inject chosen plaintext at the beginning of the secure
      communication, potentially compromising the security of data transmission. If 
      exploited, this vulnerability can lead to unauthorized access to sensitive 
      data, data manipulation, and potentially full system compromise depending on 
      the data and operations protected by the TLS session.

      To mitigate this vulnerability, disable unsafe renegotiation in the Java 
      application. Ensure that only secure renegotiation is allowed by setting the 
      system property `sun.security.ssl.allowUnsafeRenegotiation` to `false`. 

      Secure code example:
      ```
      public void safe() {
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", false);
      }
      ```
    severity: "WARNING"
    metadata:
      likelihood: "LOW"
      impact: "MEDIUM"
      confidence: "MEDIUM"
      category: "security"
      cwe: "CWE-319"
      shortDescription: "Cleartext transmission of sensitive information"
      owasp:
        - "A3:2017-Sensitive Data Exposure"
        - "A02:2021-Cryptographic Failures"
      security-severity: "MEDIUM"
      references:
        - "https://www.oracle.com/java/technologies/javase/tlsreadme.html"
      subcategory:
        - "vuln"
      technology:
        - "java"
      vulnerability: "Insecure Transport"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "Mishandled Sensitive Information"
