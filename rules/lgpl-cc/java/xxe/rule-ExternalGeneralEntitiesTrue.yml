# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://semgrep.dev/playground/r/rxTyLdl/java.lang.security.audit.xxe.documentbuilderfactory-external-general-entities-true.documentbuilderfactory-external-general-entities-true
# yamllint enable
---
rules:
  - id: java_xxe_rule-ExternalGeneralEntitiesTrue
    severity: "WARNING"
    languages:
      - "java"
    metadata:
      cwe: "CWE-611"
      shortDescription: "Improper restriction of XML external entity reference"
      owasp:
        - "A4:2017-XML External Entities (XXE)"
        - "A05:2021-Security Misconfiguration"
      security-severity: "MEDIUM"
      references:
        - "https://semgrep.dev/blog/2022/xml-security-in-java"
        - "https://semgrep.dev/docs/cheat-sheets/java-xxe/"
        - "https://blog.sonarsource.com/secure-xml-processor"
      category: "security"
      technology:
        - "java"
        - "xml"
      cwe2022-top25: "true"
      cwe2021-top25: "true"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "XML Injection"
    message: |
      This rule identifies instances in the code where an XML parser is
      configured to allow external general entities. Allowing the processing 
      of external general entities without proper validation or restriction 
      can lead to XXE attacks. An attacker can craft an XML document to 
      reference sensitive files on the server or external systems under the 
      attacker's control, leading to data disclosure, denial of service, or 
      server-side request forgery (SSRF).

      To mitigate this vulnerability, the application should be configured to 
      disable the use of external general entities in XML parsing. This can be 
      achieved by setting the feature 
      "http://xml.org/sax/features/external-general-entities" to false.

      Secure Code Example:
      ```
      class GoodSAXBuilder {
          public void GoodSAXBuilder1() throws ParserConfigurationException {
              SAXBuilder saxBuilder = new SAXBuilder();
              saxBuilder.setFeature("http://xml.org/sax/features/external-general-entities", false);
          }
      }
      ```
    pattern: |
      $PARSER.setFeature("http://xml.org/sax/features/external-general-entities",
      true);
