# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
  - id: python_flask_rule-tainted-sql-string
    languages:
      - python
    message: |
      Detected user input used to manually construct a SQL string. This is usually
      bad practice because manual construction could accidentally result in a SQL
      injection. An attacker could use a SQL injection to steal or modify contents
      of the database. Instead, use a parameterized query which is available
      by default in most database engines. Alternatively, consider using an
      object-relational mapper (ORM) such as SQLAlchemy which will protect your queries.

      SQL Injections are a critical type of vulnerability that can lead to data 
      or system compromise. By dynamically generating SQL query strings, user 
      input may be able to influence the logic of an SQL statement. 
      This could lead to an malicious parties accessing information they should not 
      have access to, or in some circumstances, being able to execute OS functionality
      or code.

      Replace all dynamically generated SQL queries with parameterized queries. 
      In situations where dynamic queries must be created, never use direct user input,
      but instead use a map or dictionary of valid values and resolve them using a user 
      supplied key.

      For example, some database drivers do not allow parameterized queries for 
      `>` or `<` comparison operators. In these cases, do not use a user supplied 
      `>` or `<` value, but rather have the user supply a `gt` or `lt` value. 
      The alphabetical values are then used to look up the `>` and `<` values to be used 
      in the construction of the dynamic query. The same goes for other queries where 
      column or table names are required but cannot be parameterized.
      Data that is possible user-controlled from a python request is passed
      to `execute()` function. To remediate this issue, use SQLAlchemy statements
      which are built with query parameterization and therefore not vulnerable 
      to sql injection.

      If for some reason this is not feasible, ensure calls including user-supplied 
      data pass it in to the `params` parameter of the `execute()` method.
      Below is an example using `execute()`, passing in user-supplied data as `params`. 
      This will treat the query as a parameterized query and `params` as strictly data, 
      preventing any possibility of SQL Injection.

      ```
      name = request.args.get('name')
      req = text('SELECT * FROM student WHERE firstname = :x')
      result = db.session.execute(req, {"x":name})
      ```
      For more information on QuerySets see:
      - https://docs.djangoproject.com/en/4.2/ref/models/querysets/#queryset-api
      For more information on SQL Injections see OWASP:
      - https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html
    metadata:
      security-severity: High
      cwe: CWE-89
      owasp:
        - "A1:2017-Injection"
        - "A03:2021-Injection"
      category: security
      shortDescription:
        Improper neutralization of special elements used in an SQL
        Command ('SQL Injection')
    mode: taint
    pattern-sources:
      - pattern-either:
          - pattern: flask.request.$ANYTHING
          - patterns:
              - pattern-inside: |
                  @$APP.route(...)
                  def $FUNC(..., $ROUTEVAR, ...):
                    ...
              - pattern: $ROUTEVAR
    pattern-sinks:
      - patterns:
          - pattern-either:
              - pattern: |
                  <... "$QUERYLIKE" ...>
          - metavariable-regex:
              metavariable: $QUERYLIKE
              regex: .*\b(?i)(SELECT .+ FROM|WHERE|DELETE FROM|INSERT INTO| CREATE TABLE|UPDATE .+ SET|ALTER TABLE|ALTER COLUMN|DROP COLUMN|DROP TABLE|ORDER BY)\b.*
    severity: ERROR
